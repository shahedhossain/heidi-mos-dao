package biz.shahed.freelance.heidi.mos.repo;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public interface DataAccesObject<T> {

	Long countAll();
	
	Long countAll(Map<String, Object> where);
	
	Long countAll(T where);
	
	Long countAllByHiberQuery(String countHQL);

	Long countAllByHiberQuery(String countHQL, Map<String, Object> where);

	Long countAllByHiberQuery(String countHQL, T where);
	
	Long countAllByNameQuery(String countNamedQuery);
	
	Long countAllByNameQuery(String countNamedQuery, Map<String, Object> where);
	
	Long countAllByNameQuery(String countNamedQuery, T where);

	Long countAllByNativeQuery(String countSQL);

	Long countAllByNativeQuery(String countSQL, Map<String, Object> where);
	
	Long countAllByNativeQuery(String countSQL, T where);
	
	List<T> delete(List<T> list);
	
	T delete(T domain);

	List<T> deleteById(List<Serializable> serializables);

	T deleteById(Serializable serializable);

	T find(Map<String, Object> where);

	T find(T where);
	
	List<T> findAll();
	
	List<T> findAll(int limit, int offset);
	
	List<T> findAll(Map<String, Object> where);
	
	List<T> findAll(Map<String, Object> where, int limit, int offset);
	
	List<T> findAll(T where);
	
	List<T> findAll(T where, int limit, int offset);
	
	List<T> findAllByHiberQuery(String hql);
	
	List<T> findAllByHiberQuery(String hql, int limit, int offset);
	
	List<T> findAllByHiberQuery(String hql, Map<String, Object> where);

	List<T> findAllByHiberQuery(String hql, Map<String, Object> where, int limit, int offset);
	
	List<T> findAllByHiberQuery(String hql, T where);
	
	List<T> findAllByHiberQuery(String hql, T where, int limit, int offset);
	
	List<T> findAllByNameQuery(String namedQuery);
	
	List<T> findAllByNameQuery(String namedQuery, int limit, int offset);
	
	List<T> findAllByNameQuery(String namedQuery, Map<String, Object> where);
	
	List<T> findAllByNameQuery(String namedQuery, Map<String, Object> where, int limit, int offset);
	
	List<T> findAllByNameQuery(String namedQuery, T where);
	
	List<T> findAllByNameQuery(String namedQuery, T where, int limit, int offset);
	
	List<T> findAllByNativeQuery(String sql);
	
	List<T> findAllByNativeQuery(String sql, int limit, int offset);
	
	List<T> findAllByNativeQuery(String sql, Map<String, Object> where);
	
	List<T> findAllByNativeQuery(String sql, Map<String, Object> where, int limit, int offset);
	
	List<T> findAllByNativeQuery(String sql, T where);
	
	List<T> findAllByNativeQuery(String sql, T where, int limit, int offset);
	
	T findByHiberQuery(String hql);
	
	T findByHiberQuery(String hql, Map<String, Object> where);
	
	T findByHiberQuery(String hql, T where);
	
	T findById(Serializable serializable);
	
	T findByNameQuery(String namedQuery);
	
	T findByNameQuery(String namedQuery, Map<String, Object> where);
	
	T findByNameQuery(String namedQuery, T where);
	
	T findByNativeQuery(String sql);
	
	T findByNativeQuery(String sql, Map<String, Object> where);
	
	T findByNativeQuery(String sql, T where);
	
	List<T> get();
	
	List<T> get(int limit, int offset);
	
	T get(Integer integer);
	
	T get(Serializable serializable);
	
	List<T> get(T where);
	
	List<T> get(T where, int limit, int offset);
	
	List<T> save(List<T> list);
	
	T save(T domain);
	
	List<T> update(List<T> list);
	
	T update(T domain);	

}
