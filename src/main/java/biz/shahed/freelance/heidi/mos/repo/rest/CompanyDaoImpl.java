package biz.shahed.freelance.heidi.mos.repo.rest;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import biz.shahed.freelance.heidi.mos.model.rest.Company;
import biz.shahed.freelance.heidi.mos.repo.SimpleDataAccessObject;


@Repository
public class CompanyDaoImpl extends SimpleDataAccessObject<Company> implements CompanyDao {
	
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(CompanyDaoImpl.class);

	public CompanyDaoImpl() {
		super();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Company> findAll() throws DataAccessException {
		Session session = getSession();
		List<Company> list = new ArrayList<Company>();		
		list = session.createCriteria(Company.class).list();
		return list;
	}

}
