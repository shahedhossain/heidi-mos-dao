package biz.shahed.freelance.heidi.mos.repo.security;

import java.util.List;

import biz.shahed.freelance.heidi.mos.model.security.Role;
import biz.shahed.freelance.heidi.mos.repo.DataAccesObject;

public interface RoleDao extends DataAccesObject<Role> {
	
	public List<Role> findAll();
	
}
