package biz.shahed.freelance.heidi.mos.repo.rest;

import java.util.List;

import biz.shahed.freelance.heidi.mos.model.rest.Department;
import biz.shahed.freelance.heidi.mos.repo.DataAccesObject;

public interface DepartmentDao extends DataAccesObject<Department> {
	
	List<Department> findAll();
	
}
