package biz.shahed.freelance.heidi.mos.repo.rest;

import java.util.List;

import biz.shahed.freelance.heidi.mos.model.rest.City;
import biz.shahed.freelance.heidi.mos.repo.DataAccesObject;

public interface CityDao extends DataAccesObject<City> {
	
	List<City> findAll();
	
}
