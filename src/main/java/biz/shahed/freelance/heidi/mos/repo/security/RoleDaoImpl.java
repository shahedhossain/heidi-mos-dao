package biz.shahed.freelance.heidi.mos.repo.security;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import biz.shahed.freelance.heidi.mos.model.security.Role;
import biz.shahed.freelance.heidi.mos.repo.SimpleDataAccessObject;


@Repository
public class RoleDaoImpl extends SimpleDataAccessObject<Role> implements RoleDao {
	
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(RoleDaoImpl.class);

	public RoleDaoImpl() {
		super();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Role> findAll() throws DataAccessException {
		Session session = getSession();
		List<Role> list = new ArrayList<Role>();		
		list = session.createCriteria(Role.class).list();
		return list;
	}

}
