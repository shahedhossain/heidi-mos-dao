package biz.shahed.freelance.heidi.mos.repo;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import biz.shahed.freelance.heidi.mos.util.EntityUtil;

public class SimpleDataAccessObject<T> implements DataAccesObject<T> {

	protected Class<T> clazz;
	
	@Autowired
	@Qualifier("sessionFactory")
	private SessionFactory sessionFactory;
	
	public SimpleDataAccessObject(){
		this.clazz = getDomainClass();
	}
	
	@Override
	public Long countAll() {
		Criteria criteria = getSession().createCriteria(this.clazz);
		criteria.setProjection(Projections.rowCount());
		return (Long) criteria.uniqueResult();
	}
	
	@Override
	public Long countAll(Map<String, Object> where) {
		Criteria criteria = getSession().createCriteria(this.clazz);
		EntityUtil.addRestrictions(criteria, where);
		criteria.setProjection(Projections.rowCount());
		return (Long) criteria.uniqueResult();
	}
	
	@Override
	public Long countAll(T where) {
		Criteria criteria = getSession().createCriteria(this.clazz);
		criteria = EntityUtil.addExample(criteria, where);
		criteria.setProjection(Projections.rowCount());
		return (Long) criteria.uniqueResult();
	}
	
	@Override
	public Long countAllByHiberQuery(String countHQL) {
		Query query = getSession().createQuery(countHQL);
		return (Long) query.uniqueResult();
	}
	
	@Override
	public Long countAllByHiberQuery(String countHQL, Map<String, Object> where) {
		Query query = getSession().createQuery(countHQL);
		return (Long) query.setProperties(where).uniqueResult();
	}
	
	@Override
	public Long countAllByHiberQuery(String countHQL, T where) {
		EntityUtil.filterEntity(where);
		Query query = getSession().createQuery(countHQL);
		return (Long) query.setProperties(where).uniqueResult();
	}
	
	@Override
	public Long countAllByNameQuery(String countNamedQuery) {
		Query query = getSession().getNamedQuery(countNamedQuery);
		return (Long) query.uniqueResult();
	}
	
	@Override
	public Long countAllByNameQuery(String countNamedQuery, Map<String, Object> where) {
		Query query = getSession().getNamedQuery(countNamedQuery);
		return (Long) query.setProperties(where).uniqueResult();
	}
	
	@Override
	public Long countAllByNameQuery(String countNamedQuery, T where) {
		EntityUtil.filterEntity(where);
		Query query = getSession().getNamedQuery(countNamedQuery);
		return (Long) query.setProperties(where).uniqueResult();
	}
	
	@Override
	public Long countAllByNativeQuery(String countSQL) {
		Query query = getSession().createSQLQuery(countSQL);
		return (Long) query.uniqueResult();
	}
	
	@Override
	public Long countAllByNativeQuery(String countSQL, Map<String, Object> where) {
		Query query = getSession().createSQLQuery(countSQL);
		return (Long) query.setProperties(where).uniqueResult();
	}
	
	@Override
	public Long countAllByNativeQuery(String countSQL, T where) {
		EntityUtil.filterEntity(where);
		Query query = getSession().createSQLQuery(countSQL);
		return (Long) query.setProperties(where).uniqueResult();
	}
	
	@Override
	public List<T> delete(List<T> list) {
		for (T domain : list) {
			delete(domain);
		}
		return list;
	}
	
	@Override
	public T delete(T domain) {
		getSession().delete(domain);
		return domain;
	}
	
	@Override
	public List<T> deleteById(List<Serializable> serializables) {
		List<T> list = new ArrayList<T>();
		for(Serializable serializable:serializables){
			T domain = deleteById(serializable);
			list.add(domain);
		}
		return list;
	}
	
	@Override
	public T deleteById(Serializable serializable) {
		T domain = findById(serializable);
		delete(domain);
		return domain;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public T find(Map<String, Object> where) {
		Criteria criteria = getSession().createCriteria(this.clazz);
		EntityUtil.addRestrictions(criteria, where);
		return (T)criteria.uniqueResult();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public T find(T where) {
		Criteria criteria = getSession().createCriteria(this.clazz);
		criteria = EntityUtil.addExample(criteria, where);
		return (T)criteria.uniqueResult();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<T> findAll() {
		Session session = getSession();
		Criteria criteria = session.createCriteria(this.clazz);
		return criteria.list();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<T> findAll(int limit, int offset) {
		Session session = getSession();
		Criteria criteria = session.createCriteria(this.clazz);
		criteria.setMaxResults(limit).setFirstResult(offset);
		return criteria.list();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<T> findAll(Map<String, Object> where) {
		Criteria criteria = getSession().createCriteria(this.clazz);
		EntityUtil.addRestrictions(criteria, where);
		return criteria.list();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<T> findAll(Map<String, Object> where, int limit, int offset) {
		Criteria criteria = getSession().createCriteria(this.clazz);
		EntityUtil.addRestrictions(criteria, where);
		criteria.setMaxResults(limit).setFirstResult(offset);
		return criteria.list();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<T> findAll(T where) {
		Criteria criteria = getSession().createCriteria(this.clazz);
		criteria = EntityUtil.addExample(criteria, where);
		return criteria.list();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<T> findAll(T where, int limit, int offset) {
		Criteria criteria = getSession().createCriteria(this.clazz);
		criteria = EntityUtil.addExample(criteria, where);
		criteria.setMaxResults(limit).setFirstResult(offset);
		return criteria.list();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<T> findAllByHiberQuery(String hql) {
		Query query = getSession().createQuery(hql);
		return query.list();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<T> findAllByHiberQuery(String hql, int limit, int offset) {
		Query query = getSession().createQuery(hql);
		query.setMaxResults(limit).setFirstResult(offset);
		return query.list();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<T> findAllByHiberQuery(String hql, Map<String, Object> where) {
		Query query = getSession().createQuery(hql);
		return query.setProperties(where).list();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<T> findAllByHiberQuery(String hql, Map<String, Object> where, int limit, int offset) {
		Query query = getSession().createQuery(hql);
		query.setMaxResults(limit).setFirstResult(offset);
		return query.setProperties(where).list();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<T> findAllByHiberQuery(String hql, T where) {
		EntityUtil.filterEntity(where);
		Query query = getSession().createQuery(hql);
		return query.setProperties(where).list();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<T> findAllByHiberQuery(String hql, T where, int limit, int offset) {
		EntityUtil.filterEntity(where);
		Query query = getSession().createQuery(hql);
		query.setMaxResults(limit).setFirstResult(offset);
		return query.setProperties(where).list();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<T> findAllByNameQuery(String namedQuery) {
		Query query = getSession().getNamedQuery(namedQuery);
		return query.list();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<T> findAllByNameQuery(String namedQuery, int limit, int offset) {
		Query query = getSession().getNamedQuery(namedQuery);
		query.setMaxResults(limit).setFirstResult(offset);
		return query.list();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<T> findAllByNameQuery(String namedQuery, Map<String, Object> where) {
		Query query = getSession().getNamedQuery(namedQuery);
		return query.setProperties(where).list();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<T> findAllByNameQuery(String namedQuery, Map<String, Object> where, int limit, int offset) {
		Query query = getSession().getNamedQuery(namedQuery);
		query.setMaxResults(limit).setFirstResult(offset);
		return query.setProperties(where).list();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<T> findAllByNameQuery(String namedQuery, T where) {
		EntityUtil.filterEntity(where);
		Query query = getSession().getNamedQuery(namedQuery);
		return query.setProperties(where).list();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<T> findAllByNameQuery(String namedQuery, T where, int limit, int offset) {
		EntityUtil.filterEntity(where);
		Query query = getSession().getNamedQuery(namedQuery);
		query.setMaxResults(limit).setFirstResult(offset);
		return query.setProperties(where).list();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<T> findAllByNativeQuery(String sql) {
		SQLQuery query = getSession().createSQLQuery(sql);
		query.addEntity(getDomainClass());
		return query.list();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<T> findAllByNativeQuery(String sql, int limit, int offset) {
		SQLQuery query = getSession().createSQLQuery(sql);
		query.setMaxResults(limit).setFirstResult(offset);
		query.addEntity(getDomainClass());
		return query.list();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<T> findAllByNativeQuery(String sql, Map<String, Object> where) {
		SQLQuery query = getSession().createSQLQuery(sql);
		query.addEntity(getDomainClass());
		query.setProperties(where);
		return query.list();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<T> findAllByNativeQuery(String sql, Map<String, Object> where, int limit, int offset) {
		SQLQuery query = getSession().createSQLQuery(sql);
		query.setMaxResults(limit).setFirstResult(offset);
		query.addEntity(getDomainClass());
		query.setProperties(where);
		return query.list();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<T> findAllByNativeQuery(String sql, T where) {
		EntityUtil.filterEntity(where);
		SQLQuery query = getSession().createSQLQuery(sql);
		query.addEntity(getDomainClass());
		query.setProperties(where);
		return query.list();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<T> findAllByNativeQuery(String sql, T where, int limit, int offset) {
		EntityUtil.filterEntity(where);
		SQLQuery query = getSession().createSQLQuery(sql);
		query.setMaxResults(limit).setFirstResult(offset);
		query.addEntity(getDomainClass());
		query.setProperties(where);
		return query.list();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public T findByHiberQuery(String hql) {
		Query query = getSession().createQuery(hql);
		return (T) query.uniqueResult();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public T findByHiberQuery(String hql, Map<String, Object> where) {
		Query query = getSession().createQuery(hql);
		return (T) query.setProperties(where).uniqueResult();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public T findByHiberQuery(String hql, T where) {
		EntityUtil.filterEntity(where);
		Query query = getSession().createQuery(hql);
		return (T) query.setProperties(where).uniqueResult();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public T findById(Serializable serializable) {	
		return (T) getSession().get(this.clazz, serializable);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public T findByNameQuery(String namedQuery) {
		Query query = getSession().getNamedQuery(namedQuery);
		return (T) query.uniqueResult();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public T findByNameQuery(String namedQuery, Map<String, Object> where) {
		Query query = getSession().getNamedQuery(namedQuery);
		return (T) query.setProperties(where).uniqueResult();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public T findByNameQuery(String namedQuery, T where) {
		EntityUtil.filterEntity(where);
		Query query = getSession().getNamedQuery(namedQuery);
		return (T) query.setProperties(where).uniqueResult();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public T findByNativeQuery(String sql) {
		Query query = getSession().createSQLQuery(sql);
		return (T) query.uniqueResult();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public T findByNativeQuery(String sql, Map<String, Object> where) {
		Query query = getSession().createSQLQuery(sql);
		return (T) query.setProperties(where).uniqueResult();
	}

	@Override
	@SuppressWarnings("unchecked")
	public T findByNativeQuery(String sql, T where) {
		EntityUtil.filterEntity(where);
		Query query = getSession().createSQLQuery(sql);
		return (T) query.setProperties(where).uniqueResult();
	}

	@Override
	public List<T> get() {
		return findAll();
	}

	@Override
	public List<T> get(int limit, int offset) {
		return findAll(limit, offset);
	}
	
	@Override
	public T get(Integer integer) {
		return findById(integer);
	}
	
	@Override
	public T get(Serializable serializable) {
		return findById(serializable);
	}
	
	@Override
	public List<T> get(T where) {
		return findAll(where);
	}
	
	@Override
	public List<T> get(T where, int limit, int offset) {
		return findAll(where, limit, offset);
	}
	
	@SuppressWarnings("unchecked")
	private Class<T> getDomainClass(){
		ParameterizedType type = (ParameterizedType)getClass().getGenericSuperclass();
		Type[] types = type.getActualTypeArguments();
		return (Class<T>)types[0];
	}

	protected Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	@Override
	public List<T> save(List<T> list) {
		for (T domain : list) {
			save(domain);
		}
		return list;
	}

	@Override
	public T save(T domain) {
		getSession().save(domain);
		return domain;
	}

	@Override
	public List<T> update(List<T> list) {
		for (T domain : list) {
			update(domain);
		}
		return list;
	}

	@Override
	public T update(T domain) {
		getSession().saveOrUpdate(domain);
		return domain;
	}

}
