package biz.shahed.freelance.heidi.mos.repo.rest;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import biz.shahed.freelance.heidi.mos.model.rest.Department;
import biz.shahed.freelance.heidi.mos.repo.SimpleDataAccessObject;


@Repository
public class DepartmentDaoImpl extends SimpleDataAccessObject<Department> implements DepartmentDao {
	
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(DepartmentDaoImpl.class);

	public DepartmentDaoImpl() {
		super();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Department> findAll() throws DataAccessException {
		Session session = getSession();
		List<Department> list = new ArrayList<Department>();		
		list = session.createCriteria(Department.class).list();
		return list;
	}

}
