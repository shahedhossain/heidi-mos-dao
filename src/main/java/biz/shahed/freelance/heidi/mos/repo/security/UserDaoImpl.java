package biz.shahed.freelance.heidi.mos.repo.security;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import biz.shahed.freelance.heidi.mos.model.security.User;
import biz.shahed.freelance.heidi.mos.repo.SimpleDataAccessObject;


@Repository
public class UserDaoImpl extends SimpleDataAccessObject<User> implements UserDao {
	
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(UserDaoImpl.class);

	public UserDaoImpl() {
		super();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<User> findAll() throws DataAccessException {
		Session session = getSession();
		List<User> list = new ArrayList<User>();		
		list = session.createCriteria(User.class).list();
		return list;
	}
	
	@Override
	public User findByUsername(String username) {
		User user = new User();
		user.setUsername(username);
		return findByNameQuery("User.findByUsername", user);
	}

}
