package biz.shahed.freelance.heidi.mos.repo.security;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import biz.shahed.freelance.heidi.mos.model.security.Authority;
import biz.shahed.freelance.heidi.mos.repo.SimpleDataAccessObject;


@Repository
public class AuthorityDaoImpl extends SimpleDataAccessObject<Authority> implements AuthorityDao {
	
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(AuthorityDaoImpl.class);

	public AuthorityDaoImpl() {
		super();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Authority> findAll() throws DataAccessException {
		Session session = getSession();
		List<Authority> list = new ArrayList<Authority>();		
		list = session.createCriteria(Authority.class).list();
		return list;
	}
	
	public List<Authority> findAllByUsername(String username) {
		Map<String, Object> where = new HashMap<String, Object>();
		where.put("username", username);
		return  findAllByNameQuery("Authority.findAllByUsername", where);
	}

}
