package biz.shahed.freelance.heidi.mos.repo.security;

import java.util.List;

import biz.shahed.freelance.heidi.mos.model.security.User;
import biz.shahed.freelance.heidi.mos.repo.DataAccesObject;

public interface UserDao extends DataAccesObject<User> {
	
	List<User> findAll();
	
	User findByUsername(String username);
	
}
