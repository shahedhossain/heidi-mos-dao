package biz.shahed.freelance.heidi.mos.repo.security;

import java.util.List;

import biz.shahed.freelance.heidi.mos.model.security.Authority;
import biz.shahed.freelance.heidi.mos.repo.DataAccesObject;

public interface AuthorityDao extends DataAccesObject<Authority> {
	
	List<Authority> findAll();
	
	List<Authority> findAllByUsername(String username);
	
}
