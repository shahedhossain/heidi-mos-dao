package biz.shahed.freelance.heidi.mos.repo.security;

import java.util.List;

import biz.shahed.freelance.heidi.mos.model.security.Requestmap;
import biz.shahed.freelance.heidi.mos.repo.DataAccesObject;

public interface RequestmapDao extends DataAccesObject<Requestmap> {
	
	public List<Requestmap> findAll();
	
}
