package biz.shahed.freelance.heidi.mos.repo.rest;

import java.util.List;

import biz.shahed.freelance.heidi.mos.model.rest.Worker;
import biz.shahed.freelance.heidi.mos.repo.DataAccesObject;

public interface WorkerDao extends DataAccesObject<Worker> {
	
	List<Worker> findAll();
	
}
