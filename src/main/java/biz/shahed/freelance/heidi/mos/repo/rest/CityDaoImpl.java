package biz.shahed.freelance.heidi.mos.repo.rest;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import biz.shahed.freelance.heidi.mos.model.rest.City;
import biz.shahed.freelance.heidi.mos.repo.SimpleDataAccessObject;


@Repository
public class CityDaoImpl extends SimpleDataAccessObject<City> implements CityDao {
	
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(CityDaoImpl.class);

	public CityDaoImpl() {
		super();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<City> findAll() throws DataAccessException {
		Session session = getSession();
		List<City> list = new ArrayList<City>();		
		list = session.createCriteria(City.class).list();
		return list;
	}

}
