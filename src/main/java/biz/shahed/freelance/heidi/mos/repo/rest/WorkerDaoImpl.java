package biz.shahed.freelance.heidi.mos.repo.rest;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import biz.shahed.freelance.heidi.mos.model.rest.Office;
import biz.shahed.freelance.heidi.mos.model.rest.Worker;
import biz.shahed.freelance.heidi.mos.repo.SimpleDataAccessObject;


@Repository
public class WorkerDaoImpl extends SimpleDataAccessObject<Worker> implements WorkerDao {
	
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(WorkerDaoImpl.class);

	public WorkerDaoImpl() {
		super();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Worker> findAll() throws DataAccessException {
		Session session = getSession();
		List<Worker> list = new ArrayList<Worker>();		
		list = session.createCriteria(Office.class).list();
		return list;
	}

}
