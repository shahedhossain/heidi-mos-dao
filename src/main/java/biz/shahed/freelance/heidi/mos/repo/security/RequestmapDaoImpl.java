package biz.shahed.freelance.heidi.mos.repo.security;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import biz.shahed.freelance.heidi.mos.model.security.Requestmap;
import biz.shahed.freelance.heidi.mos.repo.SimpleDataAccessObject;


@Repository
public class RequestmapDaoImpl extends SimpleDataAccessObject<Requestmap> implements RequestmapDao {
	
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(RequestmapDaoImpl.class);

	public RequestmapDaoImpl() {
		super();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Requestmap> findAll() throws DataAccessException {
		Session session = getSession();
		List<Requestmap> list = new ArrayList<Requestmap>();		
		list = session.createCriteria(Requestmap.class).list();
		return list;
	}

}
