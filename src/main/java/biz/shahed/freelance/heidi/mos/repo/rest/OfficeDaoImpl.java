package biz.shahed.freelance.heidi.mos.repo.rest;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import biz.shahed.freelance.heidi.mos.model.rest.Office;
import biz.shahed.freelance.heidi.mos.repo.SimpleDataAccessObject;


@Repository
public class OfficeDaoImpl extends SimpleDataAccessObject<Office> implements OfficeDao {
	
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(OfficeDaoImpl.class);

	public OfficeDaoImpl() {
		super();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Office> findAll() throws DataAccessException {
		Session session = getSession();
		List<Office> list = new ArrayList<Office>();		
		list = session.createCriteria(Office.class).list();
		return list;
	}

}
