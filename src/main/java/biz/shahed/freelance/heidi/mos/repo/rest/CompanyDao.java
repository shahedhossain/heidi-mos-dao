package biz.shahed.freelance.heidi.mos.repo.rest;

import java.util.List;

import biz.shahed.freelance.heidi.mos.model.rest.Company;
import biz.shahed.freelance.heidi.mos.repo.DataAccesObject;

public interface CompanyDao extends DataAccesObject<Company> {
	
	List<Company> findAll();
	
}
